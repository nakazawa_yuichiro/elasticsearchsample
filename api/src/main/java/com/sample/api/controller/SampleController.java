package com.sample.api.controller;

import com.google.common.collect.Maps;
import com.sample.api.entity.GeoSearch;
import com.sample.datasource.entity.GeoSearchEntity;
import com.sample.datasource.entity.SearchEntity;
import com.sample.datasource.service.GeoSearchService;
import com.sample.datasource.service.SearchService;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.index.IndexResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author nakazawa_yuichiro (Cyberagent,Inc.)
 */
@RestController
@RequestMapping(value = "/sample")
@Slf4j
public class SampleController {

    @Autowired
    private SearchService searchService;

    @Autowired
    private GeoSearchService geoSearchService;

    @RequestMapping(value = "{index}/{type}/{id}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public SearchEntity get(
            @PathVariable String index,
            @PathVariable String type,
            @PathVariable String id
    ) {
        return searchService.get(index, type, id);
    }

    @RequestMapping(value = "{index}/{type}", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public IndexResponse post(
            @PathVariable("index") String index,
            @PathVariable("type") String type,
            @RequestBody SearchEntity searchEntity
    ) {
        return searchService.put(index, type, searchEntity);
    }


    @RequestMapping(value = "{index}/{type}/_search", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public List<SearchEntity> get(
            @PathVariable("index") String index,
            @PathVariable("type") String type
    ) {
        return searchService.search(index, type);
    }


    @RequestMapping(value = "geo/{index}/{type}/{id}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public GeoSearchEntity getGeo(
            @PathVariable String index,
            @PathVariable String type,
            @PathVariable String id
    ) {
        return geoSearchService.get(index, type, id);
    }

    @RequestMapping(value = "geo/{index}/{type}", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public IndexResponse postGeo(
            @PathVariable("index") String index,
            @PathVariable("type") String type,
            @RequestBody GeoSearchEntity geoSearchEntity
    ) {
        return geoSearchService.put(index, type, geoSearchEntity);
    }


    @RequestMapping(value = "geo/{index}/{type}/_search", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public List<GeoSearchEntity> getGeoSearch(
            @PathVariable("index") String index,
            @PathVariable("type") String type,
            @RequestBody GeoSearch geoSearch
    ) {
        return geoSearchService.search(index, type, geoSearch.getPin(), geoSearch.getDistance());
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Map<String, String> get() {
        Map<String, String> map = Maps.newHashMap();
        map.put("sample", "hello");
        return map;
    }
}
