package com.sample.api.entity;

import com.sample.datasource.entity.Pin;
import lombok.Getter;
import lombok.Setter;

/**
 * @author nakazawa_yuichiro (Cyberagent,Inc.)
 */
@Getter
@Setter
public class GeoSearch {
    private Pin pin;
    private double distance;
}
