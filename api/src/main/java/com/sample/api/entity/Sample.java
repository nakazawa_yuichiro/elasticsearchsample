package com.sample.api.entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * @author nakazawa_yuichiro (Cyberagent,Inc.)
 */
@Getter
@Setter
public class Sample {

    @NotNull
    private String test;
}
