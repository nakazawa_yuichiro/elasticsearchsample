package com.sample.api.config;

import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.ContextHandlerCollection;
import org.eclipse.jetty.server.handler.DefaultHandler;
import org.eclipse.jetty.server.nio.SelectChannelConnector;
import org.eclipse.jetty.util.thread.QueuedThreadPool;
import org.eclipse.jetty.util.thread.ThreadPool;
import org.springframework.boot.bind.RelaxedPropertyResolver;
import org.springframework.boot.context.embedded.ErrorPage;
import org.springframework.boot.context.embedded.jetty.JettyEmbeddedServletContainer;
import org.springframework.boot.context.embedded.jetty.JettyEmbeddedServletContainerFactory;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;

import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * @author nakazawa_yuichiro (Cyberagent,Inc.)
 */
@Configuration
public class NettyConfiguration implements EnvironmentAware {

    private RelaxedPropertyResolver propertyResolver;

    private Properties properties;

    @Override
    public void setEnvironment(Environment environment) {
        propertyResolver = new RelaxedPropertyResolver(environment);
        properties = new Properties();
        Map<String, Object> subProperties = propertyResolver.getSubProperties("jetty.");
        for (String key : subProperties.keySet()) {
            properties.setProperty(key, subProperties.get(key).toString());
        }
    }

    @Bean
    public ContextHandlerCollection contextHandlerCollection() {
        ContextHandlerCollection contextHandlerCollection = new ContextHandlerCollection();
        Server server = new Server();
        server.setThreadPool(threadPool());
        server.setConnectors(new Connector[]{connector()});
        server.setHandler(new DefaultHandler());
        return contextHandlerCollection;
    }

    @Bean
    public ThreadPool threadPool() {
        BlockingQueue blockingQueue = new ArrayBlockingQueue(5000);
        QueuedThreadPool queuedThreadPool = new QueuedThreadPool(blockingQueue);
        queuedThreadPool.setMaxThreads(Integer.valueOf(properties.getProperty("maxThread")));
        queuedThreadPool.setMinThreads(Integer.valueOf(properties.getProperty("minThread")));
        return queuedThreadPool;
    }

    @Bean
    public Connector connector() {
        return new SelectChannelConnector();
    }

    @Bean
    public JettyEmbeddedServletContainerFactory jettyEmbeddedServletContainerFactory() {
        JettyEmbeddedServletContainerFactory factory = new JettyEmbeddedServletContainerFactory();
//        factory.addErrorPages(new ErrorPage(HttpStatus.BAD_REQUEST, "/WEB-INF/jsp/400.jsp"));
//        factory.addErrorPages(new ErrorPage(HttpStatus.UNAUTHORIZED, "/WEB-INF/jsp/401.jsp"));
//        factory.addErrorPages(new ErrorPage(HttpStatus.FORBIDDEN, "/WEB-INF/jsp/403.jsp"));
//        factory.addErrorPages(new ErrorPage(HttpStatus.NOT_FOUND, "/WEB-INF/jsp/404.jsp"));
//        factory.addErrorPages(new ErrorPage(HttpStatus.METHOD_NOT_ALLOWED, "/WEB-INF/jsp/405.jsp"));
//        factory.addErrorPages(new ErrorPage(HttpStatus.NOT_ACCEPTABLE, "/WEB-INF/jsp/406.jsp"));
//        factory.addErrorPages(new ErrorPage(HttpStatus.REQUEST_TIMEOUT, "/WEB-INF/jsp/408.jsp"));
//        factory.addErrorPages(new ErrorPage(HttpStatus.CONFLICT, "/WEB-INF/jsp/409.jsp"));
//        factory.addErrorPages(new ErrorPage(HttpStatus.LENGTH_REQUIRED, "/WEB-INF/jsp/411.jsp"));
//        factory.addErrorPages(new ErrorPage(HttpStatus.REQUEST_ENTITY_TOO_LARGE, "/WEB-INF/jsp/413.jsp"));
//        factory.addErrorPages(new ErrorPage(HttpStatus.UNSUPPORTED_MEDIA_TYPE, "/WEB-INF/jsp/415.jsp"));
//        factory.addErrorPages(new ErrorPage(HttpStatus.INTERNAL_SERVER_ERROR, "/WEB-INF/jsp/500.jsp"));
//        factory.addErrorPages(new ErrorPage(HttpStatus.NOT_IMPLEMENTED, "/WEB-INF/jsp/501.jsp"));
//        factory.addErrorPages(new ErrorPage(HttpStatus.BAD_GATEWAY, "/WEB-INF/jsp/502.jsp"));
//        factory.addErrorPages(new ErrorPage(HttpStatus.SERVICE_UNAVAILABLE, "/WEB-INF/jsp/503.jsp"));
        return factory;
    }
}

