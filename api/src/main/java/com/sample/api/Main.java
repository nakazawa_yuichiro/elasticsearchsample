package com.sample.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author nakazawa_yuichiro (Cyberagent,Inc.)
 */
@EnableAutoConfiguration
@Configuration
@ComponentScan(basePackages = "com.sample")
public class Main {
    public Main() {

    }
    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }
}
