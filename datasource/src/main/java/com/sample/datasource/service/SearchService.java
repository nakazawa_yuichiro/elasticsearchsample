package com.sample.datasource.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.common.collect.Lists;
import com.sample.datasource.entity.SearchEntity;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequestBuilder;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.naming.directory.SearchResult;
import java.io.IOException;
import java.util.List;

/**
 * @author nakazawa_yuichiro (Cyberagent,Inc.)
 */
@Service
@Slf4j
public class SearchService {

    @Autowired
    private Client searchClient;

    @Autowired
    @Qualifier("searchMapper")
    private ObjectMapper objectMapper;

    public IndexResponse put(String index, String type, SearchEntity entity) {
        try {
            String json = null;
            json = objectMapper.writeValueAsString(entity);
            IndexRequestBuilder indexRequestBuilder = searchClient.prepareIndex(index, type).setSource(json);
            IndexResponse indexResponse = indexRequestBuilder.execute().actionGet(1000);
            return indexResponse;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public SearchEntity get(String index, String type, String id) {
        try {
            GetResponse getResponse = searchClient.prepareGet(index, type, id).execute().actionGet(1000);
            String json = getResponse.getSourceAsString();
            return objectMapper.readValue(json, SearchEntity.class);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<SearchEntity> search(String index, String type) {
        SearchResponse searchResponse = searchClient.prepareSearch(index)
                .setTypes(type)
                .execute()
                .actionGet(1000);
        List<SearchEntity> searchEntityList = Lists.newArrayList();
        SearchHits searchHits = searchResponse.getHits();
        for (SearchHit searchHit : searchHits) {
            try {
                String json = searchHit.getSourceAsString();
                log.debug(json);
                searchEntityList.add(objectMapper.readValue(json, SearchEntity.class));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return searchEntityList;
    }

}
