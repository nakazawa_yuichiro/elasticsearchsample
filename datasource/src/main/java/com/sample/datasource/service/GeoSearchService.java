package com.sample.datasource.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.sample.datasource.entity.Location;
import com.sample.datasource.entity.Pin;
import com.sample.datasource.entity.GeoSearchEntity;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequestBuilder;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.bytes.BytesReference;
import org.elasticsearch.common.geo.GeoDistance;
import org.elasticsearch.common.unit.DistanceUnit;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

/**
 * @author nakazawa_yuichiro (Cyberagent,Inc.)
 */
@Service
@Slf4j
public class GeoSearchService {

    @Autowired
    private Client searchClient;

    @Autowired
    @Qualifier("searchMapper")
    private ObjectMapper objectMapper;

    public IndexResponse put(String index, String type, GeoSearchEntity entity) {
        try {
            String json = null;
            json = objectMapper.writeValueAsString(entity);
            IndexRequestBuilder indexRequestBuilder = searchClient.prepareIndex(index, type).setSource(json);
            IndexResponse indexResponse = indexRequestBuilder
                    .execute().actionGet(1000);
            return indexResponse;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public GeoSearchEntity get(String index, String type, String id) {

        try {
            GetResponse getResponse = searchClient.prepareGet(index, type, id).execute().actionGet(1000);
            String json = getResponse.getSourceAsString();
            return objectMapper.readValue(json, GeoSearchEntity.class);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<GeoSearchEntity> search(String index, String type, Pin pin, double distance) {
        Location location = pin.getLocation();
        SearchResponse searchResponse = searchClient.prepareSearch(index)
                .setTypes(type)
                .setPostFilter(
                        FilterBuilders.geoDistanceFilter("pin.location")
                                .point(location.getLat(), location.getLon())
                                .distance(distance, DistanceUnit.METERS)
                                .geoDistance(GeoDistance.ARC)
                )
                .execute()
                .actionGet(1000);

        List<GeoSearchEntity> searchEntityList = Lists.newArrayList();
        SearchHits searchHits = searchResponse.getHits();
        for (SearchHit searchHit : searchHits) {
            try {
                String json = searchHit.getSourceAsString();
                searchEntityList.add(objectMapper.readValue(json, GeoSearchEntity.class));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return searchEntityList;
    }

}
