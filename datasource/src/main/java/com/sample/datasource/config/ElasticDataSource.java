package com.sample.datasource.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author nakazawa_yuichiro (Cyberagent,Inc.)
 */
@Configuration
public class ElasticDataSource {

    @Bean(name = "searchClient")
    public Client client() {
        Settings settings = ImmutableSettings.settingsBuilder().put("client.transport.pign_timeout", 3000).build();
        Client client = new TransportClient(settings)
                .addTransportAddress(new InetSocketTransportAddress("a12685-elasticsearch.amb-stg-281.a4c.jp", 9300));
        return client;
    }

    @Bean(name= "searchMapper")
    public ObjectMapper objectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper;
    }
}

