package com.sample.datasource.entity;

import lombok.Getter;
import lombok.Setter;

/**
 * @author nakazawa_yuichiro (Cyberagent,Inc.)
 */
@Getter
@Setter
public class Location {
    private Double lat;
    private Double lon;
}
