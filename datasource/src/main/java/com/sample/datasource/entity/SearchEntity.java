package com.sample.datasource.entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * @author nakazawa_yuichiro (Cyberagent,Inc.)
 */
@Getter
@Setter
@JsonSerialize
public class SearchEntity {
    @NotNull
    private Integer id;
    @NotNull
    private String text;
}
