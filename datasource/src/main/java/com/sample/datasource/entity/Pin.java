package com.sample.datasource.entity;

import lombok.Getter;
import lombok.Setter;
import org.elasticsearch.index.translog.Translog;

/**
 * @author nakazawa_yuichiro (Cyberagent,Inc.)
 */
@Getter
@Setter
public class Pin {
    private Location location;
}
