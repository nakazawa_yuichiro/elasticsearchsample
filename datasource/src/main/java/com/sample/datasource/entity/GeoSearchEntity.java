package com.sample.datasource.entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.sun.jdi.Location;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * @author nakazawa_yuichiro (Cyberagent,Inc.)
 */
@Getter
@Setter
public class GeoSearchEntity {
    @NotNull
    private Integer id;
    private Pin pin;
}
